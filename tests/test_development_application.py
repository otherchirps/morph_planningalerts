import unittest
from morph_planningalerts.models import (
    DevelopmentApplication, database, MorphDatabase
)

class TestDevelopmentApplication(unittest.TestCase):

    def setUp(self):
        MorphDatabase.init(':memory:')
        self.sample_ref = 'ref123'
        self.sample_data = dict(
            council_reference=self.sample_ref,
            address='64 Zoo Lane',
            description='foo bar baz',
            info_url='http://superbad.com',
            comment_url='another url'
        )

    def tearDown(self):
        database.drop_table(DevelopmentApplication)
        MorphDatabase.close()

    def test_insert(self):
        DevelopmentApplication.create(
            **self.sample_data
        )

    def test_query(self):
        self.test_insert()

        da = DevelopmentApplication.get(
            DevelopmentApplication.council_reference==self.sample_ref
        )
        for field, expected_value in self.sample_data.items():
            self.assertEquals(expected_value, getattr(da, field))

    def test_in_query(self):
        self.test_insert()

        da = DevelopmentApplication.get(
            DevelopmentApplication.council_reference.in_([self.sample_ref, '1234', 'aaaa'])
        )
        for field, expected_value in self.sample_data.items():
            self.assertEquals(expected_value, getattr(da, field))

    def test_update(self):
        self.test_insert()
        updated_address = 'nowhere'

        da = DevelopmentApplication.get(
            DevelopmentApplication.council_reference==self.sample_ref
        )

        self.assertEquals(self.sample_data['address'], da.address)

        da.address = updated_address
        da.save()

        self.assertEquals(updated_address, da.address)

